const apiUrl = "https://localhost:5000/api/Branches";

async function getBranches() {
  const response = await fetch(apiUrl);
  const branches = await response.json();

  const branchesTable = document.getElementById("branchesTable");
  branchesTable.innerHTML = "";

  for (const branch of branches) {
    const row = document.createElement("tr");

    const branchIdCell = document.createElement("td");
    branchIdCell.textContent = branch.BranchId;
    row.appendChild(branchIdCell);

    const nameCell = document.createElement("td");
    nameCell.textContent = branch.Name;
    row.appendChild(nameCell);

    const addressCell = document.createElement("td");
    addressCell.textContent = branch.Address;
    row.appendChild(addressCell);

    const cityCell = document.createElement("td");
    cityCell.textContent = branch.City;
    row.appendChild(cityCell);

    const stateCell = document.createElement("td");
    stateCell.textContent = branch.State;
    row.appendChild(stateCell);

    const zipCodeCell = document.createElement("td");
    zipCodeCell.textContent = branch.ZipCode;
    row.appendChild(zipCodeCell);

    branchesTable.appendChild(row);
  }
}

async function addBranch() {
  const branchName = document.getElementById("branchNameInput").value;
  const branchAddress = document.getElementById("branchAddressInput").value;
  const branchCity = document.getElementById("branchCityInput").value;
  const branchState = document.getElementById("branchStateInput").value;
  const branchZipCode = document.getElementById("branchZipCodeInput").value;

  const branch = {
    Name: branchName,
    Address: branchAddress,
    City: branchCity,
    State: branchState,
    ZipCode: branchZipCode
  };

  const response = await fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(branch)
  });

  if (response.status === 201) {
    // Branch added successfully.
    getBranches();
  } else {
    // Handle error.
  }
}

window.onload = getBranches;